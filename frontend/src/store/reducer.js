import actions from './actions'
import { initialState } from './store'

function reducer(state = initialState, action) {
  // deep clone state to avoid mutating old state
  const newState = deepClone(state);
  // run action based on type, action will mutate deep cloned new state
  switch (action.type) {
    case 'INIT'                      :                   actions.init(newState, action); break;
    case 'STAY_LOGGED_IN'            :           actions.stayLoggedIn(newState, action); break;
    case 'GET_USER_SCORES'           :          actions.getUserScores(newState, action); break;
    case 'GET_USER_SCORES_DONE'      :      actions.getUserScoresDone(newState, action); break;
    case 'GET_LEADERBOARD'           :         actions.getLeaderboard(newState, action); break;
    case 'GET_LEADERBOARD_DONE'      :     actions.getLeaderboardDone(newState, action); break;
    case 'CELL_LEFT_CLICKED'         :        actions.cellLeftClicked(newState, action); break;
    case 'CELL_RIGHT_CLICKED'        :       actions.cellRightClicked(newState, action); break;
    case 'SET_USERBOX_DISPLAY_STATE' : actions.setUserboxDisplayState(newState, action); break;
    case 'LOGIN'                     :                  actions.login(newState, action); break;
    case 'SIGNUP'                    :                 actions.signup(newState, action); break;
    case 'LOGOUT'                    :                 actions.logout(newState, action); break;
    case 'LOGIN_SUCCESSFUL'          :        actions.loginSuccessful(newState, action); break;
    case 'ERROR_MESSAGE'             :           actions.errorMessage(newState, action); break;
    default: // eh, this is just to avoid the warning
  }
  // return new state
  return newState;
}

function deepClone(object) {
  return JSON.parse(JSON.stringify(object));
}

export default reducer;
