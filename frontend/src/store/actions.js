import axios from 'axios';
import jwt_decode from 'jwt-decode';
import setAuthToken from '../functions/setAuthToken';
import store from './store';

const proxy = 'https://volcanic-minesweeper-backend.herokuapp.com';

const actions = {
  stayLoggedIn(state) {
    // check for token to keep user logged in
    if (localStorage.jwtToken) {
      // set auth token header auth
      const token = localStorage.jwtToken;
      setAuthToken(token);
      // decode token and get user info
      const decoded = jwt_decode(token);
      // set current user
      state.user = decoded;
      // set userbox display state
      state.userboxDisplayState = 'loggedin';
      // get user scores
      actions.getUserScores(state);
      // check for expired token
      const currentTime = Date.now() / 1000; // to get in milliseconds
      if (decoded.exp < currentTime) {
        // logout user
        actions.logout(state);
      }
    }
  },
  getUserScores(state) {
    axios.get(proxy + '/api/users/score').then((response) => {
      store.dispatch({
        type: 'GET_USER_SCORES_DONE',
        response,
      });
    }).catch((error) => {
      store.dispatch({
        type: 'ERROR_MESSAGE',
        error,
      });
    });
  },
  getUserScoresDone(state, { response }) {
    state.user.currentScore = response.data.currentScore;
    state.user.highScore = response.data.highScore;
  },
  getLeaderboard(state) {
    axios.get(proxy + '/api/users/leaderboard').then((response) => {
      store.dispatch({
        type: 'GET_LEADERBOARD_DONE',
        response,
      });
    }).catch((error) => {
      store.dispatch({
        type: 'ERROR_MESSAGE',
        error,
      });
    });
  },
  getLeaderboardDone(state, { response }) {
    state.leaderboard = response.data;
  },
  init(state, { form = {} }) {
    // restart game state
    state.gameState = 'inprogress';
    state.grid = [];
    state.mineCount = 0;
    state.flagCount = 0;
    // create two dimensional array grid with number of rows and columns based on form or random
    const rows    = ( form.rows    || Math.ceil(Math.random() * 15) + 5  );
    const columns = ( form.columns || Math.ceil(Math.random() * 20) + 10 );
    // populate cells with mines
    for (let row = 0; row < rows; row ++) {
      state.grid.push([]);
      for (let column = 0; column < columns; column ++) {
        state.grid[row].push({
          mine: (Math.random() < 0.2),
          clicked: false,
          flagged: false,
          display: '',
        });
        if (state.grid[row][column].mine) state.mineCount ++;
      }
    }
  },
  cellLeftClicked(state, { row, column, ripple }) {
    // if game state is 'lost', end function
    if (state.gameState === 'lost') return;
    // if cell does not exist, or has already been processed, end function
    if (!state.grid[row] || !state.grid[row][column] || state.grid[row][column].clicked) return;
    // mark cell as clicked
    state.grid[row][column].clicked = true;
    // if cell is already flagged, decrease flag count before unflagging
    if (state.grid[row][column].flagged) state.flagCount --;
    // mark cell as not flagged
    state.grid[row][column].flagged = false;
    // if cell has been clicked directly by a logged in user, update score
    if (!ripple && Object.keys(state.user).length) {
      // update score in frontend
      if (state.grid[row][column].mine) {
        state.user.currentScore = 0;
      } else {
        state.user.currentScore ++;
        // update highscore
        if (state.user.highScore < state.user.currentScore) {
          state.user.highScore = state.user.currentScore;
        }
      }
      // backend call to update score
      axios.put(proxy + '/api/users/score', {
        currentScore: state.user.currentScore,
      }).then((response) => {
        // score update successful, update leaderboard
        store.dispatch({ type: 'GET_LEADERBOARD' });
      }).catch((error) => {
        // score update failed
        store.dispatch({
          type: 'ERROR_MESSAGE',
          error,
        });
      });
    }
    // check if cell has a mine
    if (state.grid[row][column].mine) {
      // mark cell with a '#'
      state.grid[row][column].display = '#';
      // set game state to 'lost'
      state.gameState = 'lost';
      // end function
      return;
    }
    // count mines in adjacent cells
    let adjacentMineCount = 0;
    if (((state.grid[row - 1] || {})[column - 1] || {}).mine) adjacentMineCount ++;
    if (((state.grid[row - 1] || {})[column - 0] || {}).mine) adjacentMineCount ++;
    if (((state.grid[row - 1] || {})[column + 1] || {}).mine) adjacentMineCount ++;
    if (((state.grid[row - 0] || {})[column + 1] || {}).mine) adjacentMineCount ++;
    if (((state.grid[row + 1] || {})[column + 1] || {}).mine) adjacentMineCount ++;
    if (((state.grid[row + 1] || {})[column - 0] || {}).mine) adjacentMineCount ++;
    if (((state.grid[row + 1] || {})[column - 1] || {}).mine) adjacentMineCount ++;
    if (((state.grid[row - 0] || {})[column - 1] || {}).mine) adjacentMineCount ++;
    state.grid[row][column].display = adjacentMineCount;
    // if cell has zero mines in adjacent cells, run function for all adjacent cells
    if (!adjacentMineCount) {
      actions.cellLeftClicked(state, { row: row - 1, column: column - 1, ripple: true });
      actions.cellLeftClicked(state, { row: row - 1, column: column - 0, ripple: true });
      actions.cellLeftClicked(state, { row: row - 1, column: column + 1, ripple: true });
      actions.cellLeftClicked(state, { row: row - 0, column: column + 1, ripple: true });
      actions.cellLeftClicked(state, { row: row + 1, column: column + 1, ripple: true });
      actions.cellLeftClicked(state, { row: row + 1, column: column - 0, ripple: true });
      actions.cellLeftClicked(state, { row: row + 1, column: column - 1, ripple: true });
      actions.cellLeftClicked(state, { row: row - 0, column: column - 1, ripple: true });
    }
    // check if game has been completed
    for (let row of state.grid) {
      for (let cell of row) {
        // if a cell has not been clicked and does not have a mine, game has not been completed
        if (!cell.clicked && !cell.mine) return;
      }
    }
    // if function has not ended, game has been completed
    state.gameState = 'won';
  },
  cellRightClicked(state, { row, column }) {
    // do not flag cells that are already clicked
    if (state.grid[row][column].clicked) return;
    // toggle flagged property on right clicked cell
    state.grid[row][column].flagged = !state.grid[row][column].flagged;
    // increase or decrease flag count
    (state.grid[row][column].flagged ? state.flagCount ++ : state.flagCount --);
  },
  setUserboxDisplayState(state, { value }) {
    state.userboxDisplayState = value;
  },
  login(state, { loginForm }) {
    // toggle loading on
    state.userRequestLoading = true;
    axios.post(proxy + '/api/users/login', loginForm).then((response) => {
      // log in successful
      store.dispatch({
        type: 'LOGIN_SUCCESSFUL',
        response,
      });
    }).catch((error) => {
      // log in failed
      store.dispatch({
        type: 'ERROR_MESSAGE',
        error,
      });
    });
  },
  signup(state, { signupForm }) {
    // toggle loading on
    state.userRequestLoading = true;
    axios.post(proxy + '/api/users/register', signupForm).then((response) => {
      // sign up successful, log in user
      store.dispatch({
        type: 'LOGIN',
        loginForm: signupForm,
      });
    }).catch((error) => {
      // sign up failed
      store.dispatch({
        type: 'ERROR_MESSAGE',
        error,
      });
    });
  },
  logout(state) {
    // remove token from local storage
    localStorage.removeItem("jwtToken");
    // remove auth header for future requests
    setAuthToken(false);
    // set current user to empty object
    state.user = {};
    // set userbox display state
    state.userboxDisplayState = 'initial';
  },
  loginSuccessful(state, { response }) {
    // get token from response
    const { token } = response.data;
    // save token in local storage
    localStorage.setItem("jwtToken", token);
    // set token to auth header
    setAuthToken(token);
    // decode token to get user data
    const decoded = jwt_decode(token);
    // set current user
    state.user = decoded;
    // toggle loading off
    state.userRequestLoading = false;
    // set userbox display state
    state.userboxDisplayState = 'loggedin';
    // get user scores
    actions.getUserScores(state);
  },
  errorMessage(state, { error }) {
    // set error message
    state.errorMessage = error.response ? error.response.data : {};
    // toggle loading off
    state.userRequestLoading = false;
  },
}

export default actions;
