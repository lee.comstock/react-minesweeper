import { createStore } from 'redux'
import reducer from './reducer'

export const initialState = {
  // describes the current game state, 'inprogress' 'lost' 'won'
  gameState: 'inprogress',
  // holds all cell data in two dimensional array grid
  grid: [],
  // stores number of mines in grid
  mineCount: 0,
  // stores number of flags in grid
  flagCount: 0,
  // initial, loginform, signupform, loggedin
  userboxDisplayState: 'initial',
  // currently logged in user
  user: {},
  // used while doing requests to backend for log in and sign up
  userRequestLoading: false,
  // error message
  errorMessage: {},
  // leaderboard of user profiles with highscores
  leaderboard: [],
}

let store = createStore(
  reducer,
  initialState,
  (window.__REDUX_DEVTOOLS_EXTENSION__ || function() {})(),
);

export default store;
