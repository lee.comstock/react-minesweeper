import React from 'react';
import './ContactBox.scss';

function ContactBox(state) {
  return (
    <div className="contactbox">
      <h2>Hire Me</h2>
      <div className="contactbox-photo"></div>
      <h2>Lee Comstock</h2>
      <p>
        Having mostly worked with Vue.js in recent years,
        I made this minesweeper game to learn React.js
      </p>
      <a href="https://gitlab.com/lee.comstock/react-minesweeper"
      target="_blank"
      rel="noopener noreferrer">Source Code</a>
      <p>Currently available for remote frontend dev work</p>
      <a href="mailto: lee_comstock@hotmail.com">Email</a>
      <a href="https://www.linkedin.com/in/lee-comstock-79333592/"
      target="_blank"
      rel="noopener noreferrer">LinkedIn</a>
      <a href="http://volcanicpenguin.st/"
      target="_blank"
      rel="noopener noreferrer">Website</a>
      <a href="https://github.com/Volcanic-Penguin"
      target="_blank"
      rel="noopener noreferrer">Github</a>
      <a href="https://gitlab.com/lee.comstock"
      target="_blank"
      rel="noopener noreferrer">Gitlab</a>
    </div>
  );
}

export default ContactBox;
