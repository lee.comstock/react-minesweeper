import React from 'react';
import store from '../../store/store'
import './Form.scss';

function Form(state) {
  return (
    <form onSubmit={(event) => newGame(event)}>
      <label>
        <input type="number"
        placeholder="random"
        name="columns"
        onChange={(event) => updateFormData(event)}/>
        columns
      </label>
      <label>
        <input type="number"
        placeholder="random"
        name="rows"
        onChange={(event) => updateFormData(event)}/>
        rows
      </label>
      <input type="submit" value="New Game"/>
    </form>
  );
}

const form = {
  columns: '',
  rows: '',
};

function updateFormData(event) {
  form[event.target.name] = event.target.value;
}

function newGame(event) {
  event.preventDefault();
  store.dispatch({
    type: 'INIT',
    form,
  });
}

export default Form;
