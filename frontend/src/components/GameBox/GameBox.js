import React from 'react';
import Form from '../Form/Form'
import Grid from '../Grid/Grid'
import './GameBox.scss';

function GameBox(state) {
  return (
    <div className="gamebox" onContextMenu={event => event.preventDefault()}>
      <Grid/>
      <Form/>
    </div>
  );
}

export default GameBox;
