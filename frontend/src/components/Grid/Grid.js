import React from 'react';
import { connect } from "react-redux";
import store from '../../store/store'
import './Grid.scss';

function Grid(state) {
  return (
    <div>
      <div className="grid">
        {state.grid.map((row, rowIndex) => (
          <div className="row" key={rowIndex}>
            {row.map((cell, cellIndex) => (
              <span className={"cell" + ((cell.clicked || cell.flagged) ? ' clicked' : '')}
              key={cellIndex}
              style={{ 'background' : cellColor(cell) }}
              onContextMenu={() => cellRightClicked(rowIndex, cellIndex)}
              onClick={() => cellLeftClicked(rowIndex, cellIndex)}>
                <div className="grid-icon">
                  {(cell.flagged ? '#' : (cell.display || ''))}
                </div>
              </span>
            ))}
          </div>
        ))}
      </div>
      <div className="mine-count">
        {
          !isNaN(state.user.currentScore) &&
          <div>
            <p>current score: {state.user.currentScore}</p>
            <p>highscore: {state.user.highScore}</p>
          </div>
        }
        <p>{state.mineCount - state.flagCount} mines left</p>
      </div>
    </div>
  );
}

function cellColor({ display, flagged }) {
  return {
    '0' : '#c0c0c0',
    '1' : '#85c6f6',
    '2' : '#61cd8d',
    '3' : '#d9b46d',
    '4' : '#9374d9',
    '5' : '#30658b',
    '6' : '#be5050',
    '7' : '#2c6033',
    '8' : '#3b1e3b',
    '#' : '#ff6868',
    'f' : '#b28080',
  }[(flagged ? 'f' : display)];
}

function cellRightClicked(row, column) {
  store.dispatch({
    type: 'CELL_RIGHT_CLICKED',
    row,
    column,
  });
}

function cellLeftClicked(row, column) {
  store.dispatch({
    type: 'CELL_LEFT_CLICKED',
    row,
    column,
  });
}

function mapStateToProps(state) {
  return {
    grid: state.grid,
    mineCount: state.mineCount,
    flagCount: state.flagCount,
    user: state.user,
  };
}

export default connect(mapStateToProps)(Grid);
