import React from 'react';
import { connect } from 'react-redux';
import store from '../../store/store';
import './UserBox.scss';

function UserBox(state) {
  switch (state.userboxDisplayState) {
    case 'initial':
      return (
        <div className="userbox">
          <div id="loginOrSignup">
            <input type="button" value="Log in"
            onClick={() => setUserboxDisplayState('loginform')}/>
            <input type="button" value="Sign Up"
            onClick={() => setUserboxDisplayState('signupform')}/>
          </div>
        </div>
      )
    case 'loggedin':
      return (
        <div className="userbox">
          <div id="loggedIn">
            <p>{state.user.name}</p>
            <input type="button" value="Log out" onClick={logout}/>
          </div>
        </div>
      )
    case 'loginform':
      return (
        <div className="userbox">
          <form id="loginForm" onSubmit={(event) => login(event)}>
            <label>Log in</label>
            <input type="input" placeholder="email" name="email"
            onChange={(event) => updateFormData(event, loginForm)}/>
            <input type="password" placeholder="password" name="password"
            onChange={(event) => updateFormData(event, loginForm)}/>
            <input type="submit" value="Log in"/>
          </form>
          <input type="button" value="<" className="userboxBackButton"
          onClick={() => setUserboxDisplayState('initial')}/>
          {(() => {
            if (state.userRequestLoading) {
              return (
                <div className="spinnerOverlay">
                  <div className="spinner"></div>
                </div>
              )
            }
          })()}
        </div>
      )
    case 'signupform':
      return (
        <div className="userbox">
          <form id="signupForm" onSubmit={(event) => signup(event)}>
            <label>Sign up</label>
            <input type="input" placeholder="name" name="name"
            onChange={(event) => updateFormData(event, signupForm)}/>
            <input type="input" placeholder="email" name="email"
            onChange={(event) => updateFormData(event, signupForm)}/>
            <input type="password" placeholder="password" name="password"
            onChange={(event) => updateFormData(event, signupForm)}/>
            <input type="password" placeholder="repeat password" name="password2"
            onChange={(event) => updateFormData(event, signupForm)}/>
            <input type="submit" value="Sign up"/>
          </form>
          <input type="button" value="<" className="userboxBackButton"
          onClick={() => setUserboxDisplayState('initial')}/>
          {(() => {
            if (state.userRequestLoading) {
              return (
                <div className="spinnerOverlay">
                  <div className="spinner"></div>
                </div>
              )
            }
          })()}
        </div>
      )
    default:
      return null;
  }
}

let loginForm = {};
let signupForm = {};

function updateFormData(event, form) {
  form[event.target.name] = event.target.value;
}

function setUserboxDisplayState(value) {
  loginForm = {};
  signupForm = {};
  store.dispatch({
    type: 'SET_USERBOX_DISPLAY_STATE',
    value,
  });
}

function login(event) {
  event.preventDefault();
  store.dispatch({
    type: 'LOGIN',
    loginForm,
  });
}

function signup(event) {
  event.preventDefault();
  store.dispatch({
    type: 'SIGNUP',
    signupForm,
  });
}

function logout() {
  store.dispatch({
    type: 'LOGOUT',
  });
}

function mapStateToProps(state) {
  return {
    userboxDisplayState: state.userboxDisplayState,
    userRequestLoading: state.userRequestLoading,
    user: state.user,
  };
}

export default connect(mapStateToProps)(UserBox);
