import React from 'react';
import { connect } from "react-redux";
import store from '../../store/store'
import GameBox from '../GameBox/GameBox'
import ContactBox from '../ContactBox/ContactBox'
import LeaderboardBox from '../LeaderboardBox/LeaderboardBox'
import UserBox from '../UserBox/UserBox'
import ErrorMessageBox from '../ErrorMessageBox/ErrorMessageBox'
import './App.scss';

function App(state) {
  return (
    <div className="app" style={{ 'background' : appBackgroundColor(state.gameState) }}>
      <GameBox/>
      <UserBox/>
      <ContactBox/>
      <LeaderboardBox/>
      <ErrorMessageBox/>
    </div>
  );
}

function appBackgroundColor(gameState) {
  return {
    'inprogress' : '#462c2c',
    'lost'       : '#e1457a',
    'won'        : '#b8e18d',
  }[gameState];
}

function mapStateToProps(state) {
  return {
    gameState: state.gameState,
  };
}

store.dispatch({ type: 'INIT' });
store.dispatch({ type: 'STAY_LOGGED_IN' });

export default connect(mapStateToProps)(App);
