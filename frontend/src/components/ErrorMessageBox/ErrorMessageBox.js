import React from 'react';
import { connect } from "react-redux";
import store from '../../store/store';
import './ErrorMessageBox.scss';

function ErrorMessageBox(state) {
  // don't show this component if there is no error message
  if (!Object.keys(state.errorMessage).length) return null;
  // otherwise show this component with error message
  return (
    <div className="errorMessageBoxOverlay">
      <div className="errorMessageBox">
        {Object.keys(state.errorMessage).map((errorKey, errorIndex) => (
          <p key={errorIndex}>{state.errorMessage[errorKey]}</p>
        ))}
        <input type="button" value="Ok" onClick={closeErrorMessageBox}/>
      </div>
    </div>
  );
}

function closeErrorMessageBox() {
  store.dispatch({
    type: 'ERROR_MESSAGE',
    error: {},
  });
}

function mapStateToProps(state) {
  return {
    errorMessage: state.errorMessage,
  };
}

export default connect(mapStateToProps)(ErrorMessageBox);
