import React from 'react';
import { connect } from "react-redux";
import store from '../../store/store';
import './LeaderboardBox.scss';

function LeaderboardBox(state) {
  return (
    <div className="leaderboardbox">
      <h2>Leaderboard</h2>
      <div className="leaderboardbox-list-container">
        <ul>
        {state.leaderboard.map((entry, index) => (
          <li key={index} className={(entry.id === state.user.id) ? 'highlighted' : ''}>
          <h3>{entry.name}</h3>
          <p>{entry.highScore}</p>
          </li>
        ))}
        </ul>
      </div>
    </div>
  );
}

function mapStateToProps(state) {
  return {
    leaderboard: state.leaderboard,
    user: state.user,
  };
}

store.dispatch({ type: 'GET_LEADERBOARD' });

export default connect(mapStateToProps)(LeaderboardBox);
