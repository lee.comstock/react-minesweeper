const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const connectDB = require('./config/db');
const passport = require("passport");
const users = require("./routes/api/users");

const port = process.env.PORT || 4000;
const app = express();

app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

connectDB();

// passport middleware
app.use(passport.initialize());
// passport config
require("./config/passport")(passport);
// routes
app.use("/api/users", users);

app.listen(port, () => console.log(`Server running on port ${port}`));
