const express = require("express");
const router = express.Router();
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const keys = require("../../config/keys");
const validateRegisterInput = require("../../validation/register");
const validateLoginInput = require("../../validation/login");
const User = require("../../models/User");

// @route POST api/users/register
// @desc Register user
// @access Public
router.post("/register", (req, res) => {
  // form validation
  const { errors, isValid } = validateRegisterInput(req.body);
  // check validation
  if (!isValid) return res.status(400).json(errors);
  // try to find user by email
  User.findOne({ email: req.body.email }).then(user => {
    // check if user already exists
    if (user) return res.status(400).json({ email: "Email already exists" });
    // user does not already exist, create a new one
    const newUser = new User({
      name: req.body.name,
      email: req.body.email,
      password: req.body.password,
    });
    // hash password before saving in database
    bcrypt.genSalt(10, (err, salt) => {
      bcrypt.hash(newUser.password, salt, (err, hash) => {
        if (err) throw err;
        newUser.password = hash;
        newUser.save().then(user => res.json(user)).catch(err => console.log(err));
      });
    });
  });
});

// @route POST api/users/login
// @desc Login user and return JWT token
// @access Public
router.post("/login", (req, res) => {
  // form validation
  const { errors, isValid } = validateLoginInput(req.body);
  // check validation
  if (!isValid) return res.status(400).json(errors);
  // shorthand constants
  const email = req.body.email;
  const password = req.body.password;
  // try to find user by email
  User.findOne({ email }).then(user => {
    // check if user exists
    if (!user) return res.status(404).json({ emailnotfound: "Email not found" });
    // user does exist, check password
    bcrypt.compare(password, user.password).then(isMatch => {
      if (isMatch) {
        // user matched, create JWT Payload
        const payload = {
          id: user.id,
          name: user.name
        };
        // sign token
        jwt.sign(
          payload,
          keys.secretOrKey,
          {
            expiresIn: 31556926 // 1 year in seconds
          },
          (err, token) => {
            res.json({
              success: true,
              token: "Bearer " + token,
            });
          }
        );
      } else {
        return res.status(400).json({ passwordincorrect: "Password incorrect" });
      }
    });
  });
});

// @route PUT api/users/score
// @desc Update user scores
// @access Public
router.put("/score", (req, res) => {
  // if no authorization token, send error message back
  if (!req.headers.authorization) {
    return res.status(403).json({ error: 'No credentials sent' });
  }
  // remove 'Bearer' from authorization token
  const token = req.headers.authorization.split(' ')[1];
  // check authorization token
  jwt.verify(
    token,
    keys.secretOrKey,
    (error, decoded) => {
      // check for invalid token error
      if (error) return res.status(400).json({ error: error.message });
      // find user by id and update
      User.findById(decoded.id, (err, user) => {
        // check for errors
        if (err) return res.status(400).json({ error: err.message });
        // check if user was found
        if (!user) return res.status(400).json({ error: 'No user found' });
        // update current score
        user.currentScore = req.body.currentScore;
        // update highscore
        if (user.highScore < user.currentScore) user.highScore = user.currentScore;
        // save user changes to database
        user.save().then(user => res.json(user)).catch(err => console.log(err));
      }).catch((err) => {
        return res.status(400).json({ err });
      });
    },
  );
});

// @route GET api/users/score
// @desc Get user scores
// @access Public
router.get("/score", (req, res) => {
  // if no authorization token, send error message back
  if (!req.headers.authorization) {
    return res.status(403).json({ error: 'No credentials sent' });
  }
  // remove 'Bearer' from authorization token
  const token = req.headers.authorization.split(' ')[1];
  // check authorization token
  jwt.verify(
    token,
    keys.secretOrKey,
    (error, decoded) => {
      // check for invalid token error
      if (error) return res.status(400).json({ error: error.message });
      // find user by id
      User.findById(decoded.id, (err, user) => {
        // check for errors
        if (err) return res.status(400).json({ error: err.message });
        // check if user was found
        if (!user) return res.status(400).json({ error: 'No user found' });
        // return user scores
        return res.json({
          currentScore: user.currentScore,
          highScore: user.highScore,
        });
      }).catch((err) => {
        return res.status(400).json({ err });
      });
    },
  );
});

// @route GET api/users/leaderboard
// @desc Get user highscore leaderboard
// @access Public
router.get("/leaderboard", (req, res) => {
  // get all users
  User.find({}, (error, users) => {
    // check for errors
    if (error) return res.status(400).json({ error: error.message });
    // get specific props and sort by highscores
    const leaderboard = users.map(user => ({
      id: user._id,
      name: user.name,
      highScore: user.highScore,
    })).sort((a, b) => b.highScore - a.highScore);
    // return leaderboard
    return res.json(leaderboard);
  });
});

module.exports = router;
